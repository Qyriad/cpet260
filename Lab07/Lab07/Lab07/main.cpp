#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <fstream>
#include <chrono>
#include <atomic>
#include <thread>
#include <future>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <cwctype>
#include <string>
#include <list>


void write(char const *path);
int calculate(std::string const * const);
int read(char const *path);
int readt(char const *path);
void display_usage();

typedef std::chrono::duration<double> double_seconds; // Fractional second time points, i.e. represent each time unit as a double

inline std::chrono::time_point<std::chrono::high_resolution_clock> time_now()
{
	// because std::chrono::time_point<std::chrono::high_resolution_clock> time = std::chrono::high_resolution_clock::now() is way too long to type
	// and I'm not going to pollute my :: namespace
	return std::chrono::high_resolution_clock::now();
}


int main(int argc, char **argv)
{
	// Get arguments into a manageable form
	std::vector<std::string> args;
	for(int i = 0; i < argc; i++)
	{
		char const *arg = argv[i];
		args.push_back(std::string(arg));
	}

	if(args.size() != 3)
	{
		display_usage();
		return 1; // Invalid number of arguments
	}

	if(args[1] == "write")
	{
		auto begin = time_now();
		write(args[2].c_str());
		auto end = time_now();

		// Alternatives
		//std::chrono::duration<double> elapsed = end - begin;
		//auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - begin);
		//duration_seconds elapsed = end - begin;

		double_seconds elapsed = std::chrono::duration_cast<double_seconds>(end - begin);
		std::cout << "Elapsed time: " << elapsed.count() << " seconds\n";
	}
	else if(args[1] == "read")
	{
		auto begin = time_now();
		int total = read(args[2].c_str());
		auto end = time_now();
		std::cout << "Total: " << total << "\nTime elapsed: " << std::chrono::duration_cast<double_seconds>(end - begin).count() << " seconds\n";
		std::cout.flush();
	}
	else if(args[1] == "readt")
	{
		auto begin = time_now();
		int total = readt(args[2].c_str());
		auto end = time_now();
		std::cout << "Total: " << total << "\nTime elapsed: " << std::chrono::duration_cast<double_seconds>(end - begin).count() << " seconds\n";
		std::cout.flush();
	}
	else
	{
		display_usage();
		return 2; // Invalid argument
	}

	return 0; // Everything went fine
}

void write(char const *path)
{
	auto file = std::ofstream(path);

	std::srand(static_cast<unsigned int>(std::time(0))); // not sure why std::time() returns a signed int anyways

	for(int i = 0; i < 200000; i++)
	{
		int n = (rand() % 50) + 2;
		std::stringstream ss;

		for(int i = 0; i < n; i++)
		{
			ss << rand() % 100;
			if(i < n - 1)
				ss << (rand() % 2 ? '+' : '-');
		}
		file << ss.str() << std::endl;
	}
}

int calculate(std::string const * const eq)
{
	std::string equation = *eq;
	int total = atoi(equation.c_str());
	int location = 0;
	int start = 0;

	while(location < equation.length())
	{
		if(isdigit(equation[location]) == false) // found a symbol
		{
			start = location + 1;
			switch(equation[location])
			{
				case '+' :
					total += atoi(&equation.c_str()[start]);
					break;
				case '-':
					total -= atoi(&equation.c_str()[start]);
					break;

			}
		}
		location++;
	}
	return total;
}

int read(char const *path)
{
	std::ifstream file(path);
	std::string input;
	int total = 0;

	while(getline(file, input))
	{
		total += calculate(&input);

	}
	return total;
}


int readt(char const *path)
{
	// Stuff both threads need
	std::mutex buffer_mtx;
	std::list<std::string> buffer;
	std::atomic<bool> done_reading = false; // Have we finished reading yet?
	
	std::thread read_thread
	([&buffer_mtx, &buffer, path, &done_reading]() -> void // Lambda syntax
	{
		// File IO thread
		std::ifstream file(path);
		std::string input;
		while(std::getline(file, input))
		{
			std::lock_guard<std::mutex> guard(buffer_mtx);
			buffer.push_back(input);
		}
		done_reading = true;
		return;
	});


	// Back to main thread
	int total = 0;

	while(!done_reading)
	{
		buffer_mtx.lock();
		if(buffer.size() != 0) // because apparently std::list::front() doesn't return nullptr if the list is empty
		{
			std::string eq_str = buffer.front();
			buffer.pop_front();
			buffer_mtx.unlock();

			total += calculate(&eq_str);
		}
		else
		{
			buffer_mtx.unlock();
		}
	}

	read_thread.join(); // Even if the thread is finished here if you don't join() the destructor will call abort()

	return total;
}

void display_usage()
{
	std::cout << "Usage: Lab07.exe [operation] [file path]\n\n"
		"Operations: \n\twrite\tWrites the data to [file path]\n"
		"\tread\tReads the data from [file path] using only the main thread\n"
		"\treadt\tReads the data from [file path] using multiple threads\n\n";
}