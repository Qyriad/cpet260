#include "GameObj.h"
#include <cassert>


Position GameObj::pos() const
{
	return this->position;
}

Position GameObj::prev_pos() const
{
	return this->previous_position;
}

void GameObj::update_pos(Position new_pos)
{
	this->previous_position = this->position;
	this->position = new_pos;
}

void GameObj::update_pos()
{
	this->previous_position = this->position;
	float new_x = this->position.x + this->dx;
	float new_y = this->position.y + this->dy;
	this->position = Position(new_x, new_y);
}

float GameObj::delta_x()
{
	return this->dx;
}

float GameObj::delta_y()
{
	return this->dy;
}

void GameObj::change_delta_x(float new_dx)
{
	this->dx = new_dx;
}

void GameObj::change_delta_y(float new_dy)
{
	this->dy = new_dy;
}

bool operator==(Position &&lhs, Position &&rhs)
{
	return lhs.x == rhs.x && lhs.y == rhs.y;
}
