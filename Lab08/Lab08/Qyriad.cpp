#include "Qyriad.hpp"
//#include "stdafx.h"

using namespace qyriad;


template<class T>
T con<T>::value()
{
	return this->value;
}


template<class T>
T qyriad::basic_historic<T>::operator=(const T &rhs)
{
	this->prev = this->cur;
	this->cur = rhs;
	return this->cur;
}

template<class T>
T basic_historic<T>::operator+(T const &rhs) const
{
	return this->cur + rhs;
}

template<class T>
T basic_historic<T>::operator+(basic_historic<T> const &rhs) const
{
	return this->cur + rhs.cur;
}

template<class T>
T historic<T>::operator=(T const &rhs)
{
	this->prev = this->cur;
	this->cur = rhs;
	return this->cur;
}

/*int historic<int>::operator+(int const &rhs) const
{
	return this->cur + rhs;
}*/