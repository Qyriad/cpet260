#pragma once

namespace qyriad
{
	template<class T>
	struct basic_historic
	{
	public:
		T prev;
		T cur;

		T operator=(T const &rhs);
		T operator+(T const &rhs) const;
		T operator+(basic_historic<T> const &rhs) const;
	};

	template<class T>
	struct historic : public basic_historic<T>
	{
		T operator=(T const &rhs);
	};

	/*template<>
	struct historic<int> : public basic_historic<int>
	{
		int operator+(int const &rhs) const;
	};*/

	template<class T>
	class con
	{
		private:
			T val;

		public:
			T value();

			void operator=(T const &rhs)
			{
				this->val = rhs;
				return;
			}
	};
}	