#pragma once

#include <Windows.h>

#include <vector>


enum class Color // Because strongly typed objects are better and I'm not going through all the static_cast-ing again
{
	Orange,
	Red,
	Blue,
	Black
};

enum class Bound
{
	Left = 1,
	Right = 1 << 1,
	Top = 1 << 2,
	Bottom = 1 << 3,
	All = Left | Right | Top | Bottom
};

constexpr inline Bound operator|(Bound lhs, Bound rhs) noexcept
{
	using T = std::underlying_type<Bound>::type;
	return static_cast<Bound>(static_cast<T>(lhs) | static_cast<T>(rhs));
}

constexpr inline Bound operator&(Bound lhs, Bound rhs) noexcept
{
	using T = std::underlying_type<Bound>::type;
	return static_cast<Bound>(static_cast<T>(lhs) & static_cast<T>(rhs));
}

struct Position
{
	float x;
	float y;

	Position(float xpos, float ypos) : x(xpos), y(ypos) {}
};

class GameObj
{
	private: 
		Position position;
		Position previous_position;
		float dx;
		float dy;

	public:
		GameObj(Position const initial_position) : position(initial_position), previous_position(initial_position) {}
		GameObj(float xpos, float ypos) : position(xpos, ypos), previous_position(xpos, ypos) {}
		GameObj(Position initial_position, float delta_x, float delta_y) : position(initial_position), previous_position(initial_position), dx(delta_x), dy(delta_y) {}
		GameObj(float xpos, float ypos, float delta_x, float delta_y) : position(xpos, ypos), previous_position(xpos, ypos), dx(delta_x), dy(delta_y) {}
		Position pos() const;
		Position prev_pos() const;
		void update_pos(Position new_pos);
		void update_pos();
		float delta_x();
		float delta_y();
		void change_delta_x(float new_delta_x);
		void change_delta_y(float new_delta_y);
		inline void invert_dx() { dx = -dx; }
		inline void invert_dy() { dy = -dy; }
		std::vector<GameObj*> CollidesWithObjects;
		Bound BoundaryCollisions;
		Color color;
};

// Use 'universal' references
bool operator==(Position &&lhs, Position &&rhs);
