// doubleBufferBouncingBall.cpp : Defines the entry point for the application.
//

/*
Double Buffering:
http://www.overclock.net/t/1291775/hey-i-want-animations-too-a-quick-tutorial-on-buffered-paint-animation
https://www.codeguru.com/cpp/w-p/vista/article.php/c15841/Using-the-Windows-VistaWindows-7-BuiltIn-Buffered-Animation-API.htm

Bouncing ball code
http://www.cs.sfu.ca/CourseCentral/166/tjd/bouncing_ball.html

Key codes
https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
*/

#include <cstdio>

#include <stdexcept>

#include "stdafx.h"
#include "doubleBufferBouncingBall.h"
#include "GameObj.h"

// double buffering API
#include <Uxtheme.h>
#pragma comment(lib, "uxtheme.lib")

#define MAX_LOADSTRING 100

#define  WM_DIE (WM_USER + 2)

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
HWND                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

#pragma region Console Stuff
namespace qyriad
{
	int wprintf_s(wchar_t const *format, ...)
	{
#ifndef _DEBUG
		return 0;
#endif
		va_list args;
		va_start(args, format);
		return vwprintf_s(format, args);
	}
}

#ifdef _DEBUG
inline void ActivateConsole()
{
	FILE *fp;
	_wfreopen_s(&fp, L"CONOUT$", L"w", stdout);
	wprintf_s(L"Console mode active\n");
}

inline void DeactivateConsole()
{
	INPUT ip;
	// Set up generic keyboard event
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	// Send {Enter}
	ip.ki.wVk = 0x0D;
	ip.ki.dwExtraInfo = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	// Release
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
}
#endif
#pragma endregion

enum COLOR : COLORREF
{
	// CREATE OTHER COLORS: http://www.rapidtables.com/web/color/RGB_Color.htm 
	ORANGE = RGB(255, 165, 0),
	RED = RGB(255, 0, 0),
	BLUE = RGB(0, 0, 255)
};

GameObj ball
(
	250.0f,
	250.0f,
	1.5f,
	0.0f
);

std::vector<GameObj> objs;

std::atomic<bool> run = true;

void GameThread(HWND hWnd)
{
	// game thread 

	// Initialize vector
	ball.BoundaryCollisions = Bound::All;
	ball.color = Color::Orange;
	GameObj ball2({400.0f, 250.0f}, -1.2f, 0.0f);
	ball2.color = Color::Blue;
	objs.push_back(ball);
	objs.push_back(ball2);

	run = true;

	// Clarification: GameObj * const is a constant pointer to a mutable GameObj
	// Contract: assert(object != nullptr);
	std::function<void(GameObj * const, RECT)> check_bounds = [](GameObj * const object, RECT rc) -> void
	{
		if(object == nullptr) throw std::invalid_argument("GameObj *object must not be nullptr");

		// References are still weird
		// GameObj obj = *object would make a copy of the value at the memory location described by `object`
		// GameObj &obj = *object makes a *reference* to the value at the memory location described by `object`
		GameObj &obj = *object;
		if((obj.BoundaryCollisions & Bound::Top) == Bound::Top)
		{
			if(obj.pos().y < 0)
			{
				obj.invert_dy();
			}
		}

		if((obj.BoundaryCollisions & Bound::Bottom) == Bound::Bottom)
		{
			if(obj.pos().y > rc.bottom - 20)
			{
				obj.invert_dy();
			}
		}

		if((obj.BoundaryCollisions & Bound::Left) == Bound::Left)
		{
			if(obj.pos().x < 0)
			{
				obj.invert_dx();
			}
		}

		if((obj.BoundaryCollisions & Bound::Right) == Bound::Right)
		{
			if(obj.pos().x > rc.right - 20)
			{
				obj.invert_dx();
			}
		}
	};
	std::function<bool(GameObj const * const, GameObj const * const)> is_colliding = [](GameObj const * const lhs, GameObj const * const rhs) -> bool
	{
		std::function<bool(float, float)> is_within_20 = [](float a, float b) -> bool
		{
			return std::fabs(a - b) <= 20.0f;
		};

		Position a = rhs->pos();
		Position b = lhs->pos();


		return is_within_20(a.x, b.x) && is_within_20(a.y, b.y);

	};

	while(run)
	{
		RECT rc;
		GetClientRect(hWnd, &rc);

		for(GameObj &obj : objs)
		{
			obj.update_pos();
			// Check collision
			// This is going to take a while so let's thread it
			auto f = std::async(std::launch::async, []() -> void
			{
				/*std::vector<Position> current;
				std::vector<Position> prev;
				for(GameObj const & const obj : objs)
				{
					current.push_back(obj.pos());
					prev.push_back(obj.prev_pos());
				}*/

				std::vector<std::tuple<int, int>> collide_events;
				
				for(int i = 0; i < objs.size(); i++)
				{
					for(int j = i + 1; j < objs.size() - 1; j++)
					{
						if(objs[i].pos() == objs[j].pos())
						{
							// Collision! 
							// Insert into vector
							collide_events.push_back(std::tuple<int, int>(i, j));
							// Reverse direction for both objects
							objs[i].invert_dx(); objs[i].invert_dy();
							objs[j].invert_dx(); objs[i].invert_dy();
						}
					}
				}

				// We have all positions, so now we need to see if any cross...
			});

			check_bounds(&obj, rc);

		}

		InvalidateRect(hWnd, NULL, TRUE);

		Sleep(20);

	} // end run 

	SendMessage(hWnd, WM_DIE, 0, 0);

	wprintf_s(L"Done\n");
}

int APIENTRY wWinMain(_In_		HINSTANCE hInstance,
					  _In_opt_	HINSTANCE /*hPrevInstance*/,
					  _In_		LPWSTR    /*lpCmdLine*/,
					  _In_		int       nCmdShow)
{


#pragma region Console Stuff
#ifdef _DEBUG
	if(AttachConsole(ATTACH_PARENT_PROCESS))
	{
		ActivateConsole();
	}
	else
	{
		AllocConsole();
		ActivateConsole();
	}
#endif
#pragma endregion Console Stuff

	// TODO: Place code here.


	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_DOUBLEBUFFERBOUNCINGBALL, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	HWND hWindow = InitInstance(hInstance, nCmdShow);

	// Perform application initialization:
	if(!hWindow)
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_DOUBLEBUFFERBOUNCINGBALL));

	// start game thread 
	std::future<void> f = std::async(std::launch::async, &GameThread, hWindow);

	MSG msg;

	// Main message loop:
	while(GetMessage(&msg, nullptr, 0, 0))
	{
		if(!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	f.wait();

	return static_cast<int>(msg.wParam);
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_DOUBLEBUFFERBOUNCINGBALL));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = NULL; // 
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_DOUBLEBUFFERBOUNCINGBALL);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
							  CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	// prevent resizing (https://stackoverflow.com/questions/3275989/disable-window-resizing-win32)
	//::SetWindowLong(hWnd, GWL_STYLE, GetWindowLong(hWnd, GWL_STYLE)&~WS_SIZEBOX);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return hWnd;
}

#pragma region PaintContent(HWND, HDC, LPRECT, bool)
//void PaintContent(HWND hwnd, HDC hdc, LPRECT rc, bool old)
//{
//	return;
//	// drawing for animation
//
//	HBRUSH hBrushWhite = (HBRUSH)GetStockObject(WHITE_BRUSH);
//	FillRect(hdc, rc, hBrushWhite);
//
//	if(old) return;
//
//	HBRUSH brush = CreateSolidBrush(COLOR::ORANGE);
//	SelectObject(hdc, brush);
//	for(GameObj &obj : objs)
//	{
//		long lx = std::lround(obj.pos().x);
//		long ly = std::lround(obj.pos().y);
//		Ellipse(hdc, lx, ly, lx + 20, ly + 20);
//	}
//	DeleteObject(brush);
//	DeleteObject(hBrushWhite);
//}
#pragma endregion

#pragma region OnPaint(HWND, bool)
//void OnPaint(HWND hwnd, bool animate)
//{
//	return;
//	PAINTSTRUCT ps;
//	BeginPaint(hwnd, &ps);
//
//	/*
//	* BufferedPaintRenderAnimation will return TRUE if this paint call is a result of WM_PAINT fired by the engine. The animation works by the buffered paint engine
//	* sending WM_PAINTS into your window. What we do here is detect if we are inside one of those WM_PAINTs. If we are, then we skip our code and say
//	* we're done here - somebody else did the painting for me! If not, then we continue to pain our content to render the animation.
//	*/
//	if(!BufferedPaintRenderAnimation(hwnd, ps.hdc))
//	{
//		RECT rc;
//		BP_ANIMATIONPARAMS params = { 0 };
//		GetClientRect(hwnd, &rc);
//
//		params.cbSize = sizeof(params);
//		params.style = BPAS_LINEAR; /* Linear transition */
//		params.dwDuration = 20; //  1000; //  g_currentState == g_drawnState ? 0 : 1000; /* If the state hasn't changed, then set duration to zero. The engine doesn't perform any animation if this is zero */
//
//		HDC hdcFrom;
//		HDC hdcTo;
//
//		/*
//		* BeginBufferedAnimation will give us a handle to two device contexts: A DC to render the previous state and a DC to render the current state.
//		* These are essentially key frames. hdcFrom is the DC to paint the initial state of the animation. hdcTo is the DC to paint the final state of the animation.
//		*
//		* hdcFrom will be NULL on the first call, only the final state is to be painted.
//		*/
//		HANIMATIONBUFFER hBuffer = BeginBufferedAnimation(hwnd, ps.hdc, &rc, BPBF_COMPATIBLEBITMAP, NULL, &params, &hdcFrom, &hdcTo);
//
//		if(hBuffer)
//		{
//			if(hdcFrom)
//				PaintContent(hwnd, hdcFrom, &rc, true);
//
//			if(hdcTo)
//				PaintContent(hwnd, hdcTo, &rc, false);
//
//			/* Here we update the drawn state to the current state*/
//			//g_drawnState = g_currentState;
//			EndBufferedAnimation(hBuffer, TRUE);
//		}
//	}
//
//	EndPaint(hwnd, &ps);
//}
#pragma endregion

void OnPaint(HWND hWnd)
{
	RECT rc;
	GetClientRect(hWnd, &rc);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

	HDC hNewDC;
	HPAINTBUFFER hBufferedPaint = BeginBufferedPaint(hdc, &rc, BPBF_COMPATIBLEBITMAP, NULL, &hNewDC);
	if(hBufferedPaint)
	{
		HBRUSH hBrushWhite = (HBRUSH) GetStockObject(WHITE_BRUSH);
		FillRect(hNewDC, &rc, hBrushWhite);

		//HBRUSH brush = CreateSolidBrush(COLOR::ORANGE);
		HBRUSH orange = CreateSolidBrush(COLOR::ORANGE);
		HBRUSH red = CreateSolidBrush(COLOR::RED);
		HBRUSH blue = CreateSolidBrush(COLOR::BLUE);
		HBRUSH black = CreateSolidBrush(RGB(255, 255, 255));
		//SelectObject(hNewDC, brush);

		for(GameObj &obj : objs)
		{
			long lx = std::lround(obj.pos().x);
			long ly = std::lround(obj.pos().y);

			switch(obj.color)
			{
				case Color::Orange:
					SelectObject(hNewDC, orange);
					break;
				case Color::Red:
					SelectObject(hNewDC, red);
					break;
				case Color::Blue:
					SelectObject(hNewDC, blue);
					break;
				case Color::Black:
					[[fallthrough]];
				default:
					SelectObject(hNewDC, black);
			}
			
			Ellipse(hNewDC, lx - 10, ly - 10, lx + 10, ly + 10);
		}
		DeleteObject(orange);
		DeleteObject(red);
		DeleteObject(blue);
		DeleteObject(hBrushWhite);

		// draw the updated window
		EndBufferedPaint(hBufferedPaint, TRUE);
	}

	EndPaint(hWnd, &ps);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		case WM_KEYDOWN:
		{
			switch(wParam)
			{
				case VK_LEFT: // arrow

					break;

				case VK_RIGHT: // arrow 

					break;

				case VK_SPACE: // bar 

					run = false;

					break;
			}

			break;
		}
		case WM_KEYUP:
			break;

		case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// Parse the menu selections:
			switch(wmId)
			{
				case IDM_ABOUT:
					DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
					break;
				case IDM_EXIT:
					run = false;
					break;
				default:
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
			break;
		case WM_PAINT:
			OnPaint(hWnd);
			break;

		case WM_CREATE:
			BufferedPaintInit();
			break;

		case WM_DESTROY:
			BufferedPaintUnInit();
			BufferedPaintStopAllAnimations(hWnd);
			PostQuitMessage(0);
			break;

		case WM_CLOSE:
			run = false;
			//return 1;
			break;

		case WM_DIE:
			DestroyWindow(hWnd);
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch(message)
	{
		case WM_INITDIALOG:
			return (INT_PTR)TRUE;

		case WM_COMMAND:
			if(LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
			{
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
	}
	return (INT_PTR)FALSE;
}